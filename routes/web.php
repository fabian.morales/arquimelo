<?php

use App\Http\Controllers\Admin\ContactController as AdminContactController;
use App\Http\Controllers\Admin\PagesController as AdminPagesController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ContactController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', [LoginController::class, 'logout'])->name("logout");

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('front.home');

Route::post('/contact/save', [ContactController::class, 'saveContact'])->name('front.contact.save');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::get('/', [AdminPagesController::class, 'showBackHome'])->name('back.home');

    Route::group(['prefix' => 'users'],  function(){
        Route::get('/', [UserController::class, 'list'])->name('back.user.list');
        Route::any('/data', [UserController::class, 'data'])->name('back.user.data');

        Route::get('/create', [UserController::class, 'create'])->name('back.user.create');
        Route::post('/store', [UserController::class, 'store'])->name('back.user.store');

        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('back.user.edit');
        Route::post('/update/{id}', [UserController::class, 'update'])->name('back.user.update');

        Route::get('/delete/{id}', [UserController::class, 'delete'])->name('back.user.delete');
    });

    Route::group(['prefix' => 'contact'], function() {
        Route::get('/', [AdminContactController::class, 'list'])->name('back.contact.list');
        Route::any('/data', [AdminContactController::class, 'data'])->name('back.contact.data');
        Route::any('/download', [AdminContactController::class, 'download'])->name('back.contact.download');
        Route::any('/view/{id}', [AdminContactController::class,'view'])->name('back.contact.view');
    });
});
