<?php

return [
    'google' => [
        'sitekey' => env('GOOGLE_RC_SITE'),
        'secret' => env('GOOGLE_RC_SECRET'),
    ],
    'mail' => [
        'enabled' => env('MAIL_ENABLED', 0),
        'admin' => env('MAIL_ADMIN_TO', 'soportemyc@gmail.com'),
        'support' => env('MAIL_SUPPORT', 'soportemyc@gmail.com'),
    ],
];
