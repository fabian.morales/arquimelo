/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  safelist: [
    'lSSlideOuter',
    'lSPager',
    'lSpg',
    'animate-fade-in',
    'animate-fade-out',
    'animate-fade-in-up',
    'animate-fade-in-left',
    'animate-fade-in-right',
    'animate-fade-in-down',
    'animate-fade-out-up',
    'animate-fade-out-right',
  ],
  theme: {
    fontFamily: {
      raleway: ['raleway']
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.neutral,
      indigo: colors.indigo,
      'red': {
        100: '#FFC2C6',
        200: '#FF858D',
        300: '#ff5c67',
        400: '#FF3341',
        500: '#FF0A1B',
        600: '#D9000D',
        700: '#b8000c',
        800: '#8f000a',
        900: '#660007',
      },
      yellow: colors.amber,
      green: colors.green,
      blue: colors.blue,
    },
    extend: {
      keyframes: {
        'fade-in': {
          '0%': {
            opacity: '0',
          },
          '100%': {
            opacity: 1,
          }
        },
        'fade-out': {
          '0%': {
            opacity: '1',
          },
          '100%': {
            opacity: 0,
          }
        },
        'fade-in-up': {
          '0%': {
            transform: 'translate3d(0, -100px, 0)',
            opacity: '0'
          },
          '100%': {
            transform: 'translate3d(0, 0, 0)',
            opacity: 1
          }
        },
        'fade-out-up': {
          '0%': {
            transform: 'translate3d(0, 0, 0)',
            opacity: '0'
          },
          '100%': {
            transform: 'translate3d(0, -100px, 0)',
            opacity: 1
          }
        },
        'fade-in-left': {
          '0%': {
            transform: 'translate3d(-400px, 0, 0)',
            opacity: '0',
          },
          '100%': {
            transform: 'translate3d(0, 0, 0)',
            opacity: 1,
          }
        },
        'fade-in-right': {
          '0%': {
            transform: 'translate3d(400px, 0, 0)',
            opacity: '0',
          },
          '100%': {
            transform: 'translate3d(0, 0, 0)',
            opacity: 1,
            height: '100%',
          }
        },
        'fade-out-right': {
          '0%': {
            transform: 'translate3d(0, 0, 0)',
            opacity: '1',
          },
          '100%': {
            transform: 'translate3d(400px, 0, 0)',
            opacity: 0,
          }
        }
      },
      animation: {
        'fade-in': 'fade-in 0.5s ease-out',
        'fade-out': 'fade-out 0.5s ease-out',
        'fade-in-up': 'fade-in-up 0.5s ease-out',
        'fade-out-up': 'fade-out-up 0.5s ease-out',
        'fade-in-left': 'fade-in-left 0.5s ease-out',
        'fade-in-right': 'fade-in-right 0.5s ease-out',
        'fade-out-right': 'fade-out-right 0.5s ease-out',
      }
    },
  },
  plugins: [],
}
