<form name="contact-form" action="{{ route('front.contact.save') }}" method="post" class="px-5 py-5 md:py-10 bg-gray-300 drop-shadow-md md:max-w-2xl lg:max-w-xl captcha-form" data-modal="{{ $dataModal }}">
    @csrf
    <p class="font-bold text-red-600 text-center text-3xl">Asesoría personalizada</p>
    <p class="p-1 text-center text-lg">Cuéntanos acerca de tu proyecto, uno de nuestros expertos se pondrá en contacto con usted para poder asesorarle.</p>
    <div class="grid grid-cols-2">
        <input type="text" name="full_name" id="full_name" placeholder="Nombre completo" required class="rounded-md m-2 p-2" />
        <input type="text" name="company" id="company" placeholder="Empresa" required class="rounded-md m-2 p-2" />
        <input type="email" name="email" id="email" placeholder="E-mail" required class="rounded-md m-2 col-span-2 p-2" />
        <input type="number" name="phone_number" id="phone_number" placeholder="Teléfono de contacto" required class="rounded-md m-2 p-2" />
        <input type="text" name="city" id="city" placeholder="Ciudad" required class="rounded-md m-2 p-2" />
        <textarea name="description" id="description" placeholder="Mensaje" required class="rounded-md m-2 col-span-2 p-2" rows="5"></textarea>
        <label class="col-span-2 text-left text-sm my-2"> <input type="checkbox" name="accept_terms" id="accept_terms" value="S" required /> Acepto la política de privacidad y tratamiento de datos personales</label>
        @if(App::environment(['staging', 'production']))
            {!! Captcha::renderButton() !!}
        @endif
        <button type="submit" class="w-full col-span-2 red-button">Enviar</button>
    </div>
</form>
