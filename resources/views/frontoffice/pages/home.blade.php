@extends('frontoffice.layouts.app')

@section('content')
    <section id="services" class="global-px global-py">
        <div class="text-gray-400 text-xl md:text-center md:text-3xl">
            &iexcl;Estamos <strong>listos</strong> para nuevos retos! <br>Utilizamos la <strong>tecnología y experiencia de todo nuestro equipo</strong>,
            para <strong>construir</strong> espacios de alta calidad.
        </div>
        <div class="grid grid-cols-1 md:flex md:grid-cols-3 mt-10">
            <div class="text-center md:text-left md:p-5 mb-5">
                <span class="block font-extrabold text-3xl md:text-5xl text-red-600">Expertos en</span>
                <span class="block font-extrabold text-3xl md:text-5xl text-gray-400">construcción</span>
            </div>
            <div class="md:col-span-2 md:p-5">
                <div class="service-item animation invisible">
                    <img src="{{ asset('front/images/project_04.jpg') }}" alt="" class="service-item-image" />
                    <div class="service-item-content">
                        <span class="service-item-tag">Servicios</span>
                        <span class="block font-extrabold text-xl mb-4">Construcción de estaciones de servicio</span>
                        <p class="mb-4">Ofrecemos la ejecución de todas las fases que componen la construcción de una estación de servicio (adecuación del terreno, instalación de tanques, montaje del canopy, la pavimentación y construcción de todas aquellas estructuras necesarias para la operación) cumpliendo con los estándares y normatividad técnica establecida por la legislación colombiana.
                        </p>
                        <a href="#modal-form" class="view-more">Solicitar más información</a>
                    </div>
                </div>

                <div class="service-item animation invisible">
                    <img src="{{ asset('front/images/project_03.jpg') }}" alt="" class="service-item-image" />
                    <div class="service-item-content">
                        <span class="service-item-tag">Servicios</span>
                        <span class="block font-extrabold text-xl mb-4">Construcción de uso comercial e institucional</span>
                        <p class="mb-4">Ejecutamos proyectos que abarcan todos los aspectos constructivos, funcionales, comodidad y almacenamiento que cumplan con las necesidades o lineamientos de nuestro cliente. Brindando asesoría profesional antes y durante el desarrollo de la obra.s
                        </p>
                        <a href="#modal-form" class="view-more">Solicitar más información</a>
                    </div>
                </div>

               <div class="service-item animation invisible">
                    <img src="{{ asset('front/images/project_02.jpg') }}" alt="" class="service-item-image" />
                    <div class="service-item-content">
                        <span class="service-item-tag">Servicios</span>
                        <span class="block font-extrabold text-xl mb-4">Remodelaciones</span>
                        <p class="mb-4">Adecuamos edificaciones comerciales e institucionales de alta, mediana y mínima complejidad, basados en los estándares técnicos, cumpliendo las normas de seguridad establecidas para el buen desempeño antes, durante y después de la construcción.
                        </p>
                        <a href="#modal-form" class="view-more">Solicitar más información</a>
                    </div>
                </div>

                <div class="service-item animation invisible">
                    <img src="{{ asset('front/images/project_01.jpg') }}" alt="" class="service-item-image" />
                    <div class="service-item-content">
                        <span class="service-item-tag">Servicios</span>
                        <span class="block font-extrabold text-xl mb-4">Estructuras metálicas</span>
                        <p class="mb-4">Somos especialistas en el suministro, fabricación y montaje de estructuras metálicas y obras civiles complementarias. Entregamos estructuras que le brindan la rigidez adecuada a las edificaciones, produciendo construcciones más ligeras, de calidad, económicas y rápidas.</p>
                        <a href="#modal-form" class="view-more">Solicitar más información</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="quotation" class="global-px py-5 md:py-20 bg-gray-100">
        <div class="grid grid-cols-1 md:grid-cols-3">
            <div class="text-center md:text-left md:p-5 mb-5">
                <span class="block font-extrabold text-3xl md:text-5xl text-red-600">Asesoría</span>
                <span class="block font-extrabold text-3xl md:text-5xl text-gray-400">Personalizada</span>
            </div>
            <div class="md:col-span-2 md:p-5 text-center text-gray-600">
                <span class="block font-extrabold text-xl md:text-3xl mb-2">Cotiza tu proyecto con nosotros</span>
                <span class="block mb-5 text-xl md:text-2xl">Estamos preparados para brindarte asesoría personalizada en línea.</span>
                <a href="#modal-form" class="red-button">
                    <img src="{{ asset('front/images/live-chat.svg') }}" alt="" class="inline-block" />
                    Contáctanos
                </a>
            </div>
        </div>
    </section>

    <section id="benefits" class="global-px py-10 md:py-20 benefits">
        <div class="text-center text-3xl md:text-5xl font-bold mb-10">
            &iquest;Porqué trabajar con nosotros?
        </div>
        <div class="grid grid-cols-2 md:grid-cols-4 mb-10">
            <div class="benefit-item">
                <div class="benefit-item-image">
                    <img src="{{ asset('front/images/benefit_02.svg') }}" class=" animation invisible inline-block mb-2 h-full" alt="" />
                </div>
                <span class="w-32 block text-xl">40 años de experiencia & garantía</span>
            </div>
            <div class="benefit-item">
                <div class="benefit-item-image">
                    <img src="{{ asset('front/images/benefit_04.svg') }}" class=" animation invisible inline-block mb-2 h-full" alt="" />
                </div>
                <span class="w-32 block text-xl">Optimización en tiempos de ejecución</span>
            </div>
            <div class="benefit-item">
                <div class="benefit-item-image">
                    <img src="{{ asset('front/images/benefit_03.svg') }}" class=" animation invisible inline-block mb-2 h-full" alt="" />
                </div>
                <span class="w-32 block text-xl">Excelente manejo de presupuesto</span>
            </div>
            <div class="benefit-item">
                <div class="benefit-item-image">
                    <img src="{{ asset('front/images/benefit_01.svg') }}" class=" animation invisible inline-block mb-2 h-full" alt="" />
                </div>
                <span class="w-32 block text-xl">Equipo de trabajo calificado</span>
            </div>
        </div>
        <div class="md:text-center mb-10">
            <a href="#modal-form" class="white-button">
                <img src="{{ asset('front/images/live-chat.svg') }}" alt="" class="inline-block" />
                Contactar a un asesor
            </a>
        </div>
    </section>

    <section id="projects" class="global-px md:py-20">
        <div class="grid grid-cols-1 md:grid-cols-4 -mt-16 md:-mt-48">
            <div class="text-center md:text-left flex flex-col justify-center md:p-5 mb-5 order-2 md:order-1">
                <span class="block font-extrabold text-3xl md:text-5xl text-red-600">Nuestros</span>
                <span class="block font-extrabold text-3xl md:text-5xl text-gray-400">Proyectos</span>
            </div>
            <div class="mb-10 md:mb-5 md:col-span-2 md:p-5 order-1 md:order-2 projects">
                <ul class="video-gallery">
                    <li>
                        <iframe class="project-iframe" src="https://www.youtube.com/embed/eyiaqNRFxzI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </li>
                    <li>
                        <iframe class="project-iframe" src="https://www.youtube.com/embed/RnHU3SQTNGE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </li>
                    <li>
                        <iframe class="project-iframe" src="https://www.youtube.com/embed/1RI4hsIOxpE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </li>
                </ul>
            </div>
        </div>

        <div class="project-gallery">
            <div class="active grid grid-cols-2 md:grid-cols-3 mb-10">
                <a href="{{ asset('front/images/eds-1.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/eds-1.jpg')"/></a>
                <a href="{{ asset('front/images/eds-2.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/eds-2.jpg')"/></a>
                <a href="{{ asset('front/images/eds-3.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/eds-3.jpg')"/></a>
                <a href="{{ asset('front/images/eds-4.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/eds-4.jpg')"/></a>
                <a href="{{ asset('front/images/eds-5.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/eds-5.jpg')"/></a>
                <a href="{{ asset('front/images/uso-comercial-0.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/uso-comercial-0.jpg')"/></a>
            </div>

            <div class="grid grid-cols-2 md:grid-cols-3 mb-10">
                <a href="{{ asset('front/images/uso-comercial-1.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/uso-comercial-1.jpg')"/></a>
                <a href="{{ asset('front/images/uso-comercial-2.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/uso-comercial-2.jpg')"/></a>
                <a href="{{ asset('front/images/uso-comercial-4.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/uso-comercial-4.jpg')"/></a>
                <a href="{{ asset('front/images/uso-comercial-5.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/uso-comercial-5.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-0.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-0.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-1.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-1.jpg')"/></a>
            </div>

            <div class="grid-cols-2 md:grid-cols-3 mb-10">
                <a href="{{ asset('front/images/remodelaciones-2.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-2.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-3.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-3.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-4.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-4.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-5.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-5.jpg')"/></a>
                <a href="{{ asset('front/images/remodelaciones-6.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/remodelaciones-6.jpg')"/></a>
                <a href="{{ asset('front/images/estructuras-01.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/estructuras-01.jpg')"/></a>
            </div>

            <div class="grid-cols-2 md:grid-cols-3 mb-10">
                <a href="{{ asset('front/images/estructuras-02.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/estructuras-02.jpg')"/></a>
                <a href="{{ asset('front/images/estructuras-03.jpg') }}" data-featherlight><img src="{{ asset('front/images/mask-gallery.png') }}" alt="" class="mask animation invisible w-full" style="background-image: url('../front/images/estructuras-03.jpg')"/></a>
            </div>
        </div>
        <div class="flex justify-end mr-5">
            <a href="#" rel="project-gallery-left" class="flex ml-3 w-10 h-10 border border-red-800 justify-center items-center text-gray-500 font-bold text-2xl rounded-md"><i class="fas fa-arrow-left"></i></a>
            <a href="#" rel="project-gallery-right" class="flex ml-3 w-10 h-10 border border-red-800 justify-center items-center text-gray-500 font-bold text-2xl rounded-md"><i class="fas fa-arrow-right"></i></a>
        </div>

        <div class="mb-10 text-center">
            <a href="#modal-form" class="red-button">
                <img src="{{ asset('front/images/live-chat.svg') }}" alt="" class="inline-block" />
                Contactar a un asesor
            </a>
        </div>
    </section>

    <section id="testimonials">
        <div class="global-px py-24 md:py-16 bg-gray-100 mb-20 testimonials">
            <div class="grid grid-cols-1 md:grid-cols-3">
                <div class="text-center md:text-left flex flex-col justify-center md:p-5 mb-10 md:mb-0">
                    <span class="block font-extrabold text-3xl md:text-5xl text-red-600">Nuestros</span>
                    <span class="block font-extrabold text-3xl md:text-5xl text-gray-400">Clientes</span>
                </div>
                <div class="md:col-span-2 md:p-5 text-right text-gray-600">
                    <ul class="testimonials-gallery">
                        <li>
                            <p class="testimonial-item">
                                Arquimelo SAS es una empresa que nos ha brindado todo el apoyo necesario en la
                                planeación y ejecución de nuestros proyectos de construcción, hemos confiado en ellos
                                por tu profesionalismo, experiencia y garantía en sus servicios prestados.
                            </p>
                            <p class="text-3xl font-bold ">Ismael González</p>
                        </li>
                        <li>
                            <p class="testimonial-item">
                                Trabajar con Arquimelo nos ha permitido lograr nuestros objetivos trazados
                                en cada proyecto, hemos contribuido al crecimiento de la región y el país; ellos se
                                caracterizan por cuidar cada detalle en la ejecución de sus obras.
                            </p>
                            <p class="text-3xl font-bold ">Eduardo Torres</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="global-px global-py mb-10 clients">
            <ul class="clients-gallery">
                <li class="p-2"><img src="{{ asset('front/images/terpel-logo.png') }}" alt="Terpel" /></li>
                <li class="p-2"><img src="{{ asset('front/images/combuscol-logo.png') }}" alt="Combuscol de colombia" /></li>
                <li class="p-2"><img src="{{ asset('front/images/harinera_logo.png') }}" alt="Harinera del valle" /></li>
                <li class="p-2"><img src="{{ asset('front/images/primax-logo.png') }}" alt="Primax" /></li>
                <li class="p-2"><img src="{{ asset('front/images/biomax-logo.png') }}" alt="Biomax" /></li>
                <li class="p-2"><img src="{{ asset('front/images/comfandi-logo.png') }}" alt="Comfandi" /></li>
                <li class="p-2"><img src="{{ asset('front/images/celsia-logo.png') }}" alt="Celsia" /></li>
            </ul>
        </div>
    </section>

    <section id="contact" class="global-px global-py text-center text-gray-400 mb-24">
        <div class="font-bold text-4xl mb-1">Comunícate con nosotros</div>
        <p class="text-2xl mb-1">Te acompañaremos en la construcción de tu proyecto</p>
        <p class="text-2xl text-red-600 font-bold mb-2 flex items-center justify-center">
            <img src="{{ asset('front/images/live-chat.png') }}" alt="" class="inline-block mr-2 w-14" />
            <a href="tel:+573225134626" target="_blank">
                <span>+ 57 (322) 513-4626</span>
            </a>
        </p>
        <ul class="list-none inline-block">
            <li class="inline-block p-1"><a href="https://wa.link/mr1hn7" target="_blank" class="social-button"><i class="fab fa-whatsapp"></i></a></li>
            <li class="inline-block p-1"><a href="tel:+573225134626" target="_blank" class="social-button"><i class="fas fa-phone"></i></a></li>
            <li class="inline-block p-1"><a href="mailto:servicioalcliente@arquimelo.com" target="_blank" class="social-button"><i class="far fa-envelope"></i></a></li>
        </ul>
    </section>
@endsection

@section('modals')
<div id="modal-form" class="fixed top-0 left-0 h-full w-full z-100 pt-10 hidden items-start justify-center bg-black bg-opacity-80" tabindex="-1" role="dialog">
    <div class="relative bg-white m-5 w-5/6 md:w-1/4">
        <button type="button" class="close absolute top-1 right-2 font-bold text-2xl z-10" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <div id="app-check-points">
            @include('frontoffice.components.contact_form', ['dataModal' => 'true'])
        </div>
    </div>
</div>
@endsection
