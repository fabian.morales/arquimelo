<!doctype html>
{{-- @inject('widget', 'App\Services\WidgetsService') --}}
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WQMK38X');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Arquimelo') }}</title>

    <!-- Styles -->
    <link href="{{ asset('front/css/fontawesome/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('front/js/lightslider/css/lightslider.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('front/js/featherlight/featherlight.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('front/css/app.css') }}" rel="stylesheet">

    @section('css')
    @show

    <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQMK38X"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="global-px global-py min-h-screen relative" id="home">
    <div class="absolute left-0 top-0 w-full h-screen overflow-hidden z-0 header-gradient">
        <ul class="images-banner">
            <li><img src="{{ asset('front/images/header.jpg') }}" class="h-screen md:h-auto md:w-screen max-w-none" alt="" /></li>
{{--            <li><img src="{{ asset('front/images/header.jpg') }}" class="h-screen md:h-auto md:w-screen max-w-none" alt="" /></li>--}}
{{--            <li><img src="{{ asset('front/images/header.jpg') }}" class="h-screen md:h-auto md:w-screen max-w-none" alt="" /></li>--}}
        </ul>
    </div>
    <div class="grid grid-cols-2 md:grid-cols-3 z-20 relative">
        <div class="logo">
            <img src="{{ asset('front/images/logo.svg  ') }}" alt="Arquimelo Grupo Constructor" />
        </div>
        <div class="main-menu md:col-span-2 flex justify-end">
            <a class="text-white block md:hidden text-3xl mt-5 responsive-menu" href="#"><i class="fas fa-bars"></i></a>
            <nav class="hidden md:block fixed md:static w-full md:w-auto top-0 left-0 bg-black md:bg-transparent p-2 md:p-0 z-30">
                <a href="#" class="block md:hidden absolute top-0 right-0 text-white text-5xl p-2 close-menu"><i class="fas fa-times"></i></a>
                <ul class="md:flex md:items-center">
                    <li><a href="#home">Inicio</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#projects">Proyectos</a></li>
                    <li><a href="#contact">Contacto</a></li>
                    <li class="hidden md:inline-block">
                        <a class="social-button" href="https://wa.link/mr1hn7" target="_blank"><img src="{{ asset('front/images/live-chat.svg') }}" alt="" class="inline-block w-4/5" /></a>
                    </li>
                </ul>
            </nav>
            <a class="md:hidden fixed md:static social-button top-40 right-0" href="https://wa.link/mr1hn7" target="_blank"><img src="{{ asset('front/images/live-chat.svg') }}" alt="" class="inline-block w-4/5" /></a>
        </div>
    </div>
    @section('header')
    <div class="grid grid-cols-1 md:grid-cols-2 z-10 relative mt-5">
        <div class="flex flex-col justify-center items-center text-center mb-5 px-5 py-24 md:py-5">
            <span class="block text-white font-extrabold uppercase text-4xl md:text-5xl">Construyendo lugares</span>
            <span class="block text-white text-2xl md:text-3xl">pensados en las personas</span>
        </div>
        <div class="flex flex-col items-end px-2">
        @include('frontoffice.components.contact_form', ['dataModal' => 'false'])
        </div>
    </div>
    @show
</header>

<main>
    @yield('content')
</main>

<footer class="py-5 bg-gray-500 text-white text-sm text-center">
    {{ config('app.name') }} &copy; Todos los derechos reservados {{ date('Y') }}
</footer>

@section('modals')
@show


<script src="{{ asset('front/js/jquery-3.6.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('front/js/lightslider/js/lightslider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('front/js/featherlight/featherlight.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('front/js/app.js') }}" type="text/javascript"></script>
@section('js')
@show

@if(App::environment(['staging', 'production']))
{!! Captcha::renderJs() !!}
@endif

</body>
</html>
