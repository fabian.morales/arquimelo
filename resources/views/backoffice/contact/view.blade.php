@extends('backoffice.layouts.base')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
       {{ __("Detalle de solicitud de contacto") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Solicitudes de contacto") }}
    @endslot
@endcomponent
@stop

@section('css')
@stop

@section('js')
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{ __("Detalle de solicitud de contacto") }}</h6>
    </div>
    <div class="card-body">
        <br />
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tbody>
                    <tr>
                        <td><strong>{{ __("Nombre") }}: </strong></td>
                        <td>{{ $contact->full_name }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Correo") }}: </strong></td>
                        <td>{{ $contact->email }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Teléfono") }}: </strong></td>
                        <td>{{ $contact->phone_number }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Empresa") }}: </strong></td>
                        <td>{{ $contact->company }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Ciudad") }}: </strong></td>
                        <td>{{ $contact->city }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Fecha") }}: </strong></td>
                        <td>{{ $contact->created_at }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Descripción") }}: </strong></td>
                        <td>{{ $contact->description }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ __("Aceptación de política de privacidad y tratamiento de datos personales") }}: </strong></td>
                        <td>{{ $contact->description == 'S' ? 'Sí' : 'No' }}</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <a href="{{ route('back.contact.list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">{{ __("Regresar") }}</a>
            </a>
        </div>
    </div>
</div>
@stop
