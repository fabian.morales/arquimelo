@extends('backoffice.layouts.base')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        @if($edit)
        <h1>{{ __("Editar usuario") }}</h1>
        @else
        <h1>{{ __("Crear usuario") }}</h1>
        @endif
    @endslot

    @slot('breadcrumb')
        {{ __("Usuarios") }}
    @endslot
@endcomponent
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('back/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('back/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
    (($) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.select2').select2({
            theme: 'bootstrap4'
        });
    })(jQuery);
    </script>
@stop

@section('content')

<form method="post" action="{{ $edit ? route('back.user.update', ['id' => $user->id]) : route('back.user.store') }}" class="form-row">
    <input type="hidden" id="id" name="id" value="{{ $user->id }}" />
    @csrf

    <div class="container">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ __("Datos básicos") }}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>{{ __("Nombre") }}</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ? old('name') : $user->name }}" required="required" />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Correo") }}</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') ? old('email') : $user->email }}" required="required" />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>{{ __("Clave") }}</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') ? old('password') : '' }}" />
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">&nbsp;</div>

                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label>{{ __("Rol") }}</label>
                            <select class="form-control select2 @error('role_id') is-invalid @enderror" name="role_id" id="role_id">
                                <option value="">{{ __("Seleccione un rol") }}</option>
                                @foreach($roles as $role)
                                <option value="{{ $role->id }}" @if($role->id == (old('role_id') ? old('role_id') : $user->role_id)) selected @endif>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('role_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('back.user.list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">{{ __("Regresar") }}</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">{{ __("Guardar") }}</a>
                </button>
            </div>
        </div>
    </div>
</form>
<br />
@stop
