@extends('backoffice.layouts.base')

@section('content_header')
@component('backoffice.components.content_header')
    @slot('title')
        {{ __("Usuarios") }}
    @endslot

    @slot('breadcrumb')
        {{ __("Usuarios") }}
    @endslot
@endcomponent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('back/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@stop

@section('js')
    <!-- Select2 -->
    <script src="{{ asset('back/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('back/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    (($) => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(() => {
            let hookDelete = () => {
                $("a[rel='delete']").unbind();
                $("a[rel='delete']").bind('click', (e) => {
                    e.preventDefault();
                    if (confirm('{{ __("¿Está seguro de borrar este elemento?") }}')) {
                        window.location.href = $(e.currentTarget).attr('href');
                    }
                });
            };

            let $dataTable = $('#dataTable')
                .on('order.dt', () => hookDelete())
                .on('search.dt', () => hookDelete())
                .on('page.dt', () => hookDelete())
                .DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("back.user.data") }}',
                    type: 'post',
                    complete: () => {
                        //hookDataTable();
                    }
                },
                language: {
                    url: '{{ asset("back/plugins/datatables/es.json") }}'
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { actions: 'actions', name: 'actions', searchable: false, orderable: false,
                        render: (data, type, row) => {
                            let urlEdit = '{{ route("back.user.edit", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                            let urlDelete = '{{ route("back.user.delete", ["id" => ":xx:"]) }}'.replace(':xx:', row.id);
                            return `<a href="` + urlEdit + `" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Editar usuario") }}">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                            <a href="` + urlDelete + `" rel="delete" class="display-inline" data-toggle="tooltip" data-placement="top" title="{{ __("Borrar usuario") }}">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>`;
                        }
                    }
                ],
                initComplete: (settings, json) => {
                    let api = $('#dataTable').dataTable().api();
                    let $filter = $('#dataTable_filter input');
                    $('#dataTable_filter input').unbind();
                    $('#dataTable_filter input').bind('keyup', (e) => {
                        if(e.keyCode == 13) {
                            api.search($filter.val()).draw();
                        }
                    });

                    hookDelete();
                }
            });
        });
    })(jQuery);
    </script>
@stop

@section('content')
<!-- Page Heading -->

<!-- DataTables -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary float-left">{{ __("Usuarios registrados") }}</h6>
        <a href="{{ route('back.user.create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> {{ __("Crear usuario") }}</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __("Nombre") }}</th>
                        <th>{{ __("Correo") }}</th>
                        <th class="no-sort">{{ __("Acciones") }}</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    /*@foreach($users ?? '' as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a href="{{ route('back.user.edit', ['id' => $user->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar usuario">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach*/
                    @endphp
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
