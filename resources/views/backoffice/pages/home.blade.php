@extends('backoffice.layouts.base')

@section('content_header')
    @component('backoffice.components.content_header')
    @slot('title')
    {{ __("Panel de control") }}
    @endslot

    @slot('breadcrumb')
    {{ __("Panel de control") }}
    @endslot
    @endcomponent
@stop

@section('content')
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ App\Models\Contact::count() }}</h3>

                <p>{{ __("Solicitudes de asesoría") }}</p>
            </div>
            <div class="icon">
                <i class="fas fa-id-card"></i>
            </div>
            <a href="{{ route("back.contact.list") }}" class="small-box-footer">{{ __("Ver más") }} <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- ./col -->
</div>
@endsection
