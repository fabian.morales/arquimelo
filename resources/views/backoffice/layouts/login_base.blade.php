@extends('backoffice.layouts.app', ['bodyClass' => 'login-page'])

@section('content_app')

<div class="login-box">
    <div class="login-logo">
        <br />
        <br />
        <b>Gesti&oacute;n de {{ config('app.name') }}</b>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        @if(session('message_error'))
        @component('backoffice.components.alert')
            @slot('class')
            danger
            @endslot

            @slot('message')
            {{ session('message_error') }}
            @endslot
        @endcomponent
        @endif

        @if(session('message_success'))
        @component('backoffice.components.alert')
            @slot('class')
            success
            @endslot

            @slot('message')
            {{ session('message_success') }}
            @endslot
        @endcomponent
        @endif

        <!-- Main content -->
        @yield('content')
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
</div>
@endsection
