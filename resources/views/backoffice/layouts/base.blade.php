@extends('backoffice.layouts.app', ['bodyClass' => 'sidebar-mini layout-fixed'])

@section('content_app')
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-black navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ url('/') }}" class="nav-link">{{ __("Inicio") }}</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-user"></i>
                    <!--span class="badge badge-warning navbar-badge">15</span-->
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">{{ Auth::user()->name }}</span>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item">
                        <i class="fas fa-sign-out-alt mr-2"></i> {{ __("Cerrar sesión") }}
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('front.home') }}" class="dropdown-item">
                        <i class="fas fa-home mr-2"></i> {{ __("Ver sitio") }}
                    </a>
                    @php
                    /*<div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>*/
                    @endphp
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('back.home') }}" class="brand-link">
            <img src="{{ asset('back/img/logo.png') }}" alt="{{ config('app.name') }}" class="brand-image" style="opacity: .8">
            <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('back/img/default_user.png') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            @php
                list($menuItem, $menuSubitem) = App\Helpers\MenuHelper::getActiveItems();
            @endphp

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    @role('Administrador')
                    <li class="nav-item @if($menuItem == "users") menu-open @else menu-close @endif">
                        <a href="{{ route('back.user.list') }}" class="nav-link @if($menuItem == "users") active @endif">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                {{ __("Usuarios") }}
                            </p>
                        </a>
                    </li>

                    <li class="nav-item @if($menuItem == "contact") menu-open @else menu-close @endif">
                        <a href="{{ route('back.contact.list') }}" class="nav-link @if($menuItem == "contact") active @endif">
                            <i class="nav-icon fas fa-id-card"></i>
                            <p>
                                {{ __("Solicitudes de asesoria") }}
                            </p>
                        </a>
                    </li>
                    @endrole
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        @yield('content_header')

        @if(session('message_error'))
        @component('backoffice.components.alert')
            @slot('class')
            danger
            @endslot

            @slot('message')
            {{ session('message_error') }}
            @endslot
        @endcomponent
        @endif

        @if(session('message_success'))
        @component('backoffice.components.alert')
            @slot('class')
            success
            @endslot

            @slot('message')
            {{ session('message_success') }}
            @endslot
        @endcomponent
        @endif

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; {{ date('Y') }} <a href="{{ url('/') }}">{{ config('app.name') }}</a></strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
@endsection
