<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" value="" class="form-control @error('g-recaptcha-response') is-invalid @enderror" />
@error('g-recaptcha-response')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror