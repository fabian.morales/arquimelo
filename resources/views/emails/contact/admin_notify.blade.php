@extends('emails.layouts.admin_master')

@section('content')
    @component('emails.components.title')
        @slot('text')
            {{ __("Se ha recibido una nueva solicitud de contacto") }}
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            {{ __("Estos son los datos del contacto:") }}:
        @endslot
    @endcomponent

    <ul>
        <li><b>{{ __("Nombre") }}</b>: {{ $contact->full_name }}</li>
        <li><b>{{ __("Correo") }}</b>: {{ $contact->email }}</li>
        <li><b>{{ __("Teléfono") }}</b>: {{ $contact->phone_number }}</li>
        <li><b>{{ __("Empresa") }}</b>: {{ $contact->company }}</li>
        <li><b>{{ __("Ciudad") }}</b>: {{ $contact->city }}</li>
        <li><b>{{ __("Descripción") }}</b>: {{ $contact->description }}</li>
        <li><b>Aceptación de política de privacidad y tratamiento de datos personales</b>: {{ $contact->accept_terms == 'S' ? 'Sí' : 'No' }}</li>
    </ul>
@stop
