@extends('backoffice.layouts.login_base')

@section('content')
<div class="card-body login-card-body">
    <p class="login-box-msg">{{ __('Iniciar sesión') }}</p>

    <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="input-group mb-3">
            <input id="email" type="email"  placeholder="Correo" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
        </div>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="input-group mb-3">
            <input id="password" type="password" placeholder="Clave" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-eye" id="show"></span>
                </div>
            </div>
        </div>
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="row">
            <div class="col-8">
                <div class="icheck-primary">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="remember">
                        {{ __('Recordarme') }}
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">{{ __('Entrar') }}</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <p class="mb-1">
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}">
            {{ __('¿Olvidaste tu Contraseña?') }}
        </a>
        @endif
    </p>
    <p class="mb-0">
        <!-- a href="register.html" class="text-center">Register a new membership</a-->
    </p>
</div>
@endsection
