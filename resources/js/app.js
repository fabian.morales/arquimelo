(($, $w) => {

    // const init = () => {
    //     const container = document.querySelector(".benefits-slides");
    //     const current = document.querySelector(".benefits-slides > .slide-active");
    //     container.style.height = current.offsetHeight + "px";
    // }

    const options = {
        rootMargin: '0px 0px 20px 0px',
    }

    let observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.classList.remove("invisible");
                entry.target.classList.add("animate-fade-in-up");
                observer.unobserve(entry.target);
            }
        });
    }, options);

    document.querySelectorAll('.animation.invisible').forEach(card => observer.observe(card));

    $(() => {
        $("a.responsive-menu").on('click', e => {
            e.preventDefault();
            const $this = $(e.currentTarget);
            $this.next().removeClass("hidden").addClass("animate-fade-in-right");
        });

        $("a.close-menu").on("click", e => {
            e.preventDefault();
            const $target = $(e.currentTarget).parents('nav');
            $target.addClass("animate-fade-out-right");
            setTimeout(() => {
                $target.addClass("hidden").removeClass("animate-fade-out-right");
            }, 400);

        });

        $(".main-menu nav a:not(.social-button)").on("click", e => {
            e.preventDefault();
            const target = $(e.currentTarget).attr('href');
            window.scrollTo(0, $(target)[0].getBoundingClientRect().y);
        });

        $(".images-banner").lightSlider({
            item: 1,
            controls: false,
            mode: 'fade',
            auto: true,
            loop: true,
            pause: 4000,
            pager: false,
        });

        $(".video-gallery").lightSlider({
            item: 1,
            controls: false,
            mode: 'slide',
            auto: false,
            loop: true,
            pause: 4000,
            pager: true,
        });

        $(".testimonials-gallery").lightSlider({
            item: 1,
            controls: false,
            mode: 'slide',
            auto: true,
            loop: true,
            pause: 4000,
            pager: false,
        });

        $(".clients-gallery").lightSlider({
            item: 4,
            auto: true,
            loop: true,
            pager: false,
            controls: true,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item: 3,
                    }
                },
                {
                    breakpoint:480,
                    settings: {
                        item: 2,
                    }
                }
            ]
        });

        $("a[rel='project-gallery-left']").on("click", e => {
            e.preventDefault();
            const current = $(".project-gallery > div.active");
            let prev = current.prev(); //$(".project-gallery > div.active + div");
            if (!prev || prev == null || prev.length == 0) {
                prev = $(".project-gallery > div:last-child");
            }
            current.removeClass("active").removeClass("grid");
            prev.addClass("active").addClass("grid");
        });

        $("a[rel='project-gallery-right']").on("click", e => {
            e.preventDefault();
            const current = $(".project-gallery > div.active");
            let next = current.next(); //$(".project-gallery > div.active + div");
            if (!next || next == null || next.length == 0) {
                next = $(".project-gallery > div:first-child");
            }
            current.removeClass("active").removeClass("grid");
            next.addClass("active").addClass("grid");
        });

        const hideModalForm = () => {
            $("#modal-form > div").addClass("animate-fade-out-up");
            $("#modal-form").addClass('animate-fade-out');
            setTimeout(() => {
                $("#modal-form").removeClass('animate-fade-out');
                $("#modal-form > div").removeClass("animate-fade-out-up");
                $("#modal-form").removeClass('flex').addClass('hidden');
            }, 400);
        }

        $("a[href='#modal-form']").on('click', e => {
            e.preventDefault();
            $("#modal-form").removeClass('hidden').addClass('flex').addClass('animate-fade-in');
            $("#modal-form > div").addClass("animate-fade-in-up");
        });

        $("button[data-dismiss='modal']").on('click', e => {
            e.preventDefault();
            hideModalForm();
        });

        const showModal = (message, title) => {
            alert(message);
        };

        $w.submitForm = e => {
            e.preventDefault();

            const $this = $(e.currentTarget);
            const form = $this[0];

            $this.find("button").prop('disabled', true);
            $this.find("button").addClass('btn-loading');

            if (form.full_name.value === '') {
                showModal("Debe ingresar su nombre");
                return;
            }

            if (form.company.value === '') {
                showModal("Debe ingresar la empresa");
                return;
            }

            if (form.phone_number.value === '') {
                showModal("Debe ingresar el número telefónico");
                return;
            }

            if (form.email.value === '') {
                showModal("Debe ingresar la dirección de correo");
                return;
            }

            if (form.city.value === '') {
                showModal("Debe ingresar la ciudad");
                return;
            }

            if (form.description.value === '') {
                showModal("Debe ingresar el mensaje de la solicitud");
                return;
            }

            if (form.accept_terms.value === '') {
                showModal("Debe aceptar la política de privacidad y tratamiento de datos personales");
                return;
            }

            $.ajax({
                url: $this.attr("action"),
                method: "post",
                dataType: "json",
                data: $this.serialize()
            })
            .done(res => {
                if (res.ok == 1) {
                    form.reset();
                    showModal("Su mensaje ha sido enviado exitosamente, nos pondremos en contacto lo m&aacute;s pronto posible.", "&iexcl;Gracias!");
                    if ($this.attr("data-modal") === "true") {
                        hideModalForm();
                    }
                }
                else {
                    showModal(res.message);
                }
            })
            .fail(() => {
                showModal("Ocurrió un error al recibir su información, por favor intente nuevamente");
            })
            .always(() => {
                $this.find("button").prop('disabled', false);
                $this.find("button").removeClass('btn-loading');
            });
        };

        //$("form[name='contact-form']").on('submit', $w.submitForm);
    });
})(jQuery, window);
