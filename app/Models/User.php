<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function separateLastName($fullname) {
        $parts = array_values(array_filter(array_merge(explode(' ', $fullname), ['']), function($value) {
            return !empty($value);
        }));
        
        $ret = [
            'name' => '',
            'last' => ''
        ];
    
        $aux = $parts;
        if (count($parts) >= 3) {
            $aux = [];
            $articles = ['EL', 'LAS', 'LOS', 'DE', 'LA', 'DOS'];
            $index = 0;
            foreach ($parts as $p) {
                $aux[$index] = (isset($aux[$index]) ? $aux[$index] . ' ' : '') . $p;
                if (!in_array(strtoupper($p), $articles)) {
                    $index++;
                }
            }
        }

        if (count($aux) >= 3) {
            $ret['name'] = implode(" ", array_slice($aux, 0, -2));
            $ret['last'] = implode(" ", array_slice($aux, -2));
        }
        else {
            list($ret['name'], $ret['last']) = array_merge($aux, ['', '']);
        }
        
        return $ret;
    }
}
