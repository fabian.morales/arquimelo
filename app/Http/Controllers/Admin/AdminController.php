<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

class AdminController extends Controller
{
    protected $moduleName;
    protected $permissionError = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionError = __('No tiene permisos para esta acción');
        $this->middleware('auth');
    }

    public function checkPermission($action) {
        $permission = $this->moduleName . '.' . $action;
        if (!\Auth::user()->can($permission)) {
            throw new UnauthorizedException(403, $this->permissionError);
        }
    }
}