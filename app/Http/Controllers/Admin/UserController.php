<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'users';
    }
    
    public function list() {
        $this->checkPermission('view');
        $users = User::all();
        return view('backoffice.user.list', compact('users'));
    }

    public function data() {
        return \DataTables::of(User::query())->make(true);
    }

    private function showForm(User $user, $edit = false) {
        if (empty($user)) {
            $user = new User();
        }

        $roles = Role::orderBy('name')->get();

        return view('backoffice.user.form', compact('user', 'edit', 'roles'));
    }

    public function create() {
        $this->checkPermission('create');
        return $this->showForm(new User());
    }

    public function edit($id) {
        $this->checkPermission('edit');
        $user = User::find($id);
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        return $this->showForm($user, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->get('password'));

        if ($user->save()) {
            $role = Role::findById($user->role_id);
            $user->assignRole($role);

            return redirect()
                ->route('back::user::list')
                ->with(['message_success' => __("Usuario guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el usuario")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'role_id' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($request->get('id'));
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        $password = $user->password;
        $user->fill($request->all());
        $user->password = $password;

        if (User::where('email', $user->email)->where('id', '<>', $user->id)->count() > 0) {
            return back()
                ->withInput()
                ->with(['message_error' => __("El correo electrónico que está usando ya está registrado.")]);
        }

        if ($request->has('password') && !empty($request->get('password'))) {
            $user->password = Hash::make($request->get('password'));
        }

        if ($user->save()) {
            $role = Role::findById($user->role_id);

            if (!$user->hasRole($role)) {
                $user->syncRoles([$role]);
            }

            return redirect()
                ->route('back::user::list')
                ->with(['message_success' => __("Usuario actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el usuario")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        $user = User::find($id);
        if (empty($user)) {
            return back()
                ->with(['message_error' => __("Usuario no encontrado")]);
        }

        if ($roles = $user->getRoleNames()) {
            foreach ($roles as $role) {
                $user->removeRole($role);
            }
        }

        if ($user->delete()) {
            DB::commit();
            return redirect()
                ->route('back::user::list')
                ->with(['message_success' => __("Usuario borrado exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar el usuario")]);
        }
    }
}
