<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContactController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'contacts';
    }

    public function data() {
        $this->checkPermission('view');
        return \DataTables::of(Contact::orderBy('created_at'))->make(true);
    }

    public function list() {
        $this->checkPermission('view');

        $contacts = Contact::all();
        return view('backoffice.contact.list', compact('contacts'));
    }

    public function view($id) {
        $this->checkPermission('view');

        $contact = Contact::where('id', $id)->first();
        if (empty($contact)) {
            return back()
                ->with(['message_error' => __("Solicitud de contacto no encontrada")]);
        }

        return view('backoffice.contact.view', compact('contact'));
    }

    public function download(Request $request) {
        $headers = [
            'Núm.',
            'Nombre',
            'Correo',
            'Teléfono',
            'Empresa',
            'Ciudad',
            'Descripción',
            'Fecha'
        ];

        $filename = "solicitudes_asesoria_" . date("Ymd_Hi") . ".csv";
        if (!is_dir(Storage::path('temp'))) {
            mkdir(Storage::path('temp'));
        }

        $file = fopen(Storage::path('temp/' . $filename), 'w+');
        fputcsv($file, array_map("utf8_decode", $headers), ";");
        $contacts = Contact::select(['id', 'full_name', 'phone_numer', 'company', 'city', 'description', 'created_at'])->all();
        foreach ($contacts ?? '' as $contact) {
            $contact->makeHidden(['updated_at']);
            fputcsv($file, array_map("utf8_decode", $contact->toArray()), ";");
        }
        fclose($file);

        return Storage::download('temp/' . $filename, $filename);
    }
}
