<?php

namespace App\Http\Controllers;

use App\Mail\ContactAdminNotification;
use App\Mail\ContactUserNotification;
use App\Models\Contact;
use App\Models\User;
use App\Rules\Recaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function saveContact(Request $request) {
        $ret = ["ok" => 0];

        try {
            $rules = [
                'full_name' => 'required',
                'company' => 'required',
                'phone_number' => 'required',
                'email' => 'required',
                'city' => 'required',
                'description' => 'required',
                'accept_terms' => 'required',
            ];

            if (App::environment(['staging', 'production'])) {
                $rules['g-recaptcha-response'] = ['required', new Recaptcha];
            }

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                throw new \Exception("Los datos no están completos");
            }

            $contact = new Contact();
            $contact->fill($request->all());
            $parts = User::separateLastName($contact->full_name);
            $contact->first_name = $parts['name'];
            $contact->last_name = $parts['last'];

            if (!$contact->save()) {
                throw new \Exception("No se pudo realizar el registro, intente nuevamente");
            }

            if (config('custom.mail.enabled') == 1) {
                Mail::to($contact->email)->send(new ContactUserNotification($contact));
                $to = explode(",", config('custom.mail.admin'));
                Mail::to($to)->send(new ContactAdminNotification($contact));
            }

            $ret["ok"] = 1;
        }
        catch (\Throwable $th) {
            $ret["message"] = $th->getMessage();
            $ret["line"] = $th->getLine();
            //$ret["stack"] = $th->getTraceAsString();
        }

        if(!$request->ajax()) {
            $message = [];
            if ($ret["ok"] == 1) {
                $message = ['message_success' => __("Gracias. Hemos recibido tu solicitud.")];
            }
            else {
                $message = ['message_error' => __("No se pudo recibir tu solicitud, intenta nuevamente.")];
            }

            $ret = redirect()
                ->route('front.home')
                ->with($message);
        }
        else{
            $ret = response()->json($ret);
        }

        return $ret;
    }
}
