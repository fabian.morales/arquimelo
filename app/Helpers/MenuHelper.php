<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class MenuHelper{
    public static function getActiveItems() {
        $items = [];

        if (Request::segment(1) == "admin") {
            $item = Request::segment(2);
            $items[] = Request::segment(2);
            if (!empty(Request::segment(3)) && !in_array(Request::segment(3), ['list', 'edit', 'create', 'save', 'delete', 'show'])) {
                $items[] = Request::segment(3);
            }
        }
        else{
            $item = Request::segment(1);
            $items[] = Request::segment(1);
            if (!empty(Request::segment(2) && !in_array(Request::segment(2), ['list', 'edit', 'create', 'save', 'delete', 'show']))) {
                $items[] = Request::segment(2);
            }
        }

        $hash = implode("-", $items);
        return [$item, $hash];
    }
}
