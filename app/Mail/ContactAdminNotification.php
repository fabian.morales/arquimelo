<?php

namespace App\Mail;

use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactAdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    //private LandingContact $contact;
    private $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $bcc = explode(",", config('custom.mail.support'));

        return $this
            ->bcc($bcc)
            ->subject(__("Nueva solicitud de contacto"))
            ->view('emails.contact.admin_notify', ['contact' => $this->contact, 'title' => __("Nueva solicitud de contacto")]);
    }
}
